$('body').addClass('ip_load');

// Open Menu
$('.jsOpenMenu').click(function () {
	$('.toogleActive').addClass('active')
	$('header').addClass('active')
	$('.collapse').removeClass('slideOutLeft')
	$('.collapse').addClass('slideInLeft')
})

// Close Menu
$('.jsCloseMenu').click(function () {
	$('.collapse').removeClass('slideInLeft')
	$('.collapse').addClass('slideOutLeft')
	setTimeout(function () {
		$('.toogleActive').removeClass('active')
		$('.collapse').removeClass('slideOutLeft')
		$('header').removeClass('active')
	}, 700)
})


$('.jsNatal').click(() => {
	this.parent.$('.jsNatal.box-cupons').toggleClass('active')
})

$('.jsSP').click(() => {
	this.parent.$('.jsSP.box-cupons').toggleClass('active')
})

$('.close').click(() => {
	$('.print-numeros p').empty()
	$('.print-numeros tbody tr').remove()
	$('.get-numeros').addClass('disabled')
})


$('.jsLinkPesquisa').click(() => {

	var cupomFiscal = $('#cupom-fiscal').val()

	if (cupomFiscal.length == 6 || cupomFiscal.length == 9) {

		window.location.href = `https://suaexperiencia.ipsos.com.br/uc/pesquisalm/?a=${cupomFiscal}`

	} else {
		$('#cupom-fiscal').addClass('error-input')

	}

})


$('#cupom-fiscal').keydown(() => {
	var cupomFiscal = $('#cupom-fiscal').val()

	if (cupomFiscal.length == 5 || cupomFiscal.length > 7) {

		$('.termos').removeClass('disabled')


	} else if (cupomFiscal.length > 5) {
		$('.termos').addClass('disabled')


	} else {
		$('.termos').addClass('disabled')

	}
})

$('#concordo').click(() => {
	if ($("#concordo").is(":checked") == true) {
		$('.gera-link .disabled').removeClass('disabled')
		$('.concordo').addClass('active')
	} else {
		$('.gera-link div').addClass('disabled')
		$('.concordo').removeClass('active')
	};
})

// $('#cupom-fiscal').change(() => {

// 	if (cupomFiscal.length < 2) {
// 		$('#cupom-fiscal').removeClass('error-input')
// 	}
// })

$("#cpf").keydown(function () {

	var cpf = document.getElementById("cpf").value.replace(/[.]/g, '').replace('-', '').replace('/', '')

	try {
		$("#cpf").unmask();
	} catch (e) { }

	var tamanho = $("#cpf").val().length;

	if (tamanho < 11) {
		$("#cpf").mask("999.999.999-99");
	} else {
		$("#cpf").mask("99.999.999/9999-99");
	}

	// ajustando foco
	var elem = this;
	setTimeout(function () {
		// mudo a posição do seletor
		elem.selectionStart = elem.selectionEnd = 10000;
	}, 0);
	// reaplico o valor para mudar o foco
	var currentValue = $(this).val();
	$(this).val('');
	$(this).val(currentValue);

	if (cpf.length == 10 || cpf.length > 10) {

		$('.get-numeros').removeClass('disabled')

	} else {

		$('.get-numeros').addClass('disabled')

	}
});


// Função validação de tamanho de página
function displayWindowSize() {

	var w = document.documentElement.clientWidth;

	if (w < 991) {

		$('.collapse a').click(() => $('body').addClass('o-hidden'))

		$('.modal').removeClass('fade')
		$('.modal').addClass('slideInLeft')

		$('.modal .close-mobile').click(() => {


			$('.modal').addClass('slideOutLeft')
			$('.modal-backdrop').remove()
			$('body').removeClass('o-hidden')

			$('#cpf').val('');
			$('.table').attr('hidden', true);
			$('.n-sorte tbody').empty();

			setTimeout(() => {
				$('.modal .close').click()
				$('.modal').removeClass('slideOutLeft')
				$('.modal').removeClass('fade')
			}, 600)


		})

	} else {

		$('.close').click(() => {
			$('#cpf').val('');
			$('.table').attr('hidden', true);
			$('.n-sorte tbody').empty();
		})

		$('.modal').addClass('fade')
		$('.modal').removeClass('slideInLeft')
		$('.modal').removeClass('slideOutLeft')

	}

}

window.addEventListener("resize", displayWindowSize)
displayWindowSize()

$('.jsClickConsult').click(() => {
	parent.window.location.href = 'https://ferramentas.ipsos.com.br/leroymerlinpesquisa/'
})

function SomenteNumero(e) {
	var tecla = (window.event) ? event.keyCode : e.which;
	if ((tecla > 47 && tecla < 58)) return true;
	else {
		if (tecla == 8 || tecla == 0) return true;
		else return false;
	}
}

function buscarNumerosSorte() {

	$('body').addClass('ip_load');

	var cpf = document.getElementById("cpf").value.replace(/[.]/g, '').replace('-', '').replace('/', '')

	var xhr = new XMLHttpRequest();

	xhr.open(
		'GET',
		`https://ferramentas.ipsos.com.br/homologacao2/api/promocoes/numeros-da-sorte/${cpf}`,
		true
	);

	xhr.onload = function () {

		$('body').removeClass('ip_load');

		var numeros = JSON.parse(this.responseText);

		var tbody = document.querySelector('table tbody');
		tbody.innerHTML = '';
		var row = '';

		if (numeros.length > 0) {

			$('.print-numeros p').remove()

			for (var index in numeros) {

				var numero = numeros[index];

				row = ` 
				<tr class="row">
					<td class="col-6 text-center">${numero.NumeroSorte}</td>
					<td class="col-6 text-center">${numero.Premio}</td>
				</tr>
				`;

				tbody.innerHTML += row;
			}

			document.querySelector('table').removeAttribute('hidden');

		} else {

			$('.print-numeros p').html('Não há Números da Sorte para o CPF/CNPJ informado!')

		}


	}

	xhr.send();
}

function carregaGanhadoresList() {

	var xhrGanhadores = new XMLHttpRequest();

	xhrGanhadores.open(
		'GET',
		`https://ferramentas.ipsos.com.br/Homologacao2/api/promocoes/ganhadores/`,
		//'http://192.168.19.4/LeroyMerlin/webapi/api/promocoes/ganhadores/',
		true
	);


	xhrGanhadores.onload = function () {

		$('body').removeClass('ip_load');

		var ganhadores = JSON.parse(this.responseText);

		// var json = JSON.parse(this.responseText);


		var tempHtml = ''		

		if (ganhadores.length > 0) {

			if (ganhadores[0].StatusCode == 200) {

				tempHtml += `
					<div class="col-lg-3">
						<div class="box-titles-ganhadores desktop">
							<div class="box-title">
								Número da Sorte
							</div>
							<div class="box-title">
								Nome ganhador
							</div>
							<div class="box-title">
								Loja da Sorte
							</div>
						</div>
					</div>
				`

				for (var index in ganhadores) {

					var g = ganhadores[index];



					if(g.TipoPremio == 2){

						tempHtml += `
							<div class="col-lg-2">
								<div class="box-premio">
									<img src="assets/img/cifrao_escuro.png" alt="">
									<div class="valor-premio">
										R$ 1.000,00
									</div>
								</div>
								<div class="box-tables-ganhadores">
									<div class="box-title">
										Número da Sorte
									</div>
									<div class="box-table numero-sorte">
										${g.NumeroSorte}
									</div>
									<div class="box-title">
										Nome ganhador
									</div>
									<div class="box-table">
										${g.Nome}
									</div>
									<div class="box-title">
										Loja da Sorte
									</div>
									<div class="box-table">
										${g.Loja}
									</div>
								</div>
							</div>
						`
						
					}else{
						tempHtml += `
							<div class="col-lg-2">
								<div class="box-premio">
									<img src="assets/img/3cifrao_escuro.png" alt="">
									<div class="valor-premio">
										R$ 500,00
									</div>
								</div>
								<div class="box-tables-ganhadores">
									<div class="box-title">
										Número da Sorte
									</div>
									<div class="box-table numero-sorte">
										${g.NumeroSorte}
									</div>
									<div class="box-title">
										Nome ganhador
									</div>
									<div class="box-table">
										${g.Nome}
									</div>
									<div class="box-title">
										Loja da Sorte
									</div>
									<div class="box-table">
										${g.Loja}
									</div>
								</div>
							</div>
						`

					}
				}

			} else {
				$('.box-sorteio').append('<h4>Ops, algo deu errado! </h4><h4>Error: ' + ganhadores[0].StatusCode + '</h4>')
			}
			
			$('.insert-premiados').append(tempHtml)
		} 
	}

	xhrGanhadores.send();
}

function geraNumeroDaSorte() {
	var url = new URL(location.href);
	$('body').removeClass('ip_load');

	var cpf = url.searchParams.get('c');
	var cupomFiscal = url.searchParams.get('cf');
	var tipoPremio = url.searchParams.get('tp');
	var nome = url.searchParams.get('n')
	var Loja = url.searchParams.get('lj')
	var lfdn = url.searchParams.get('lfdn')

	var mensagem = '';

	var nSorte = new XMLHttpRequest();

	nSorte.open(
		'GET',
		`https://ferramentas.ipsos.com.br/homologacao2/api/promocoes/gerar-numero-sorte/${cpf}/${cupomFiscal}/${tipoPremio}/${nome}/${Loja}/${lfdn}`,
		//`http://192.168.19.4/LeroyMerlin/webapi/api/promocoes/gerar-numero-sorte/${cpf}/${cupomFiscal}/${tipoPremio}/${nome}/${Loja}/${lfdn}`,
		true
	);

	nSorte.onload = function () {

		var json = JSON.parse(this.responseText);
		var p = document.getElementById('numero');

		if (json.StatusCode == 200) {

			// mensagem = 'Seu número da sorte é:'
			// $('.jsMensagem').html(mensagem)
			$('.jsNumSorte').html(`${json.NumeroSorte}`);
		}
		else {
			mensagem = 'Não foi possivel gerar seu numero da sorte'
			$('.jsMensagem').html(mensagem)
			$('.jsNumSorte').html(`${json.Error}`)
		}

	}

	nSorte.send();
}
